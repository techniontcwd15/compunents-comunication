import { Component, OnInit } from '@angular/core';
import { Service1Service } from './service1.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  data = '';

  constructor(private s1:Service1Service) {

  }

  ngOnInit() {
    this.s1.data.subscribe((newData) => {
      this.data = newData;
    })
  }
}
