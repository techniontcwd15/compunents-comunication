import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { Service1Service } from '../service1.service';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {
  @Input() amit:string = "";
  @Output() xyz:EventEmitter<string> = new EventEmitter<string>();

  constructor(private s1:Service1Service) {

  }

  ngOnInit() {

  }

  inputChanged(inputValue:string) {
    this.s1.editData(inputValue,1);
    this.xyz.emit(inputValue);
  }

}
