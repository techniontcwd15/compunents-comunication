import { Component, OnInit } from '@angular/core';
import { Service1Service } from '../service1.service';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css']
})
export class Component1Component implements OnInit {
  amitValue:string = '';
  constructor(private s1:Service1Service) {

  }

  showOutput(val) {
    console.log(val);
  }

  ngOnInit() {
  }

  inputChanged(inputValue:string) {
    this.s1.editData(inputValue,0);
    this.amitValue = inputValue;
  }

}
