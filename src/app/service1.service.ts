import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import 'rxjs';

@Injectable()
export class Service1Service {
  public data:Subject<string> = new Subject<string>();
  private arr:Array<string> = [];

  constructor() {
  }

  editData(newData:string, id:number) {
    this.arr[id] = newData;
    this.data.next(this.arr.join(''));
  }

}
